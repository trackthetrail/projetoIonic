import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { InParagrafoComponent } from './in-paragrafo/in-paragrafo';
import { BrowserModule } from '../../node_modules/@angular/platform-browser';
import { IonicModule } from '../../node_modules/ionic-angular/umd';
@NgModule({
	declarations: [InParagrafoComponent],
	imports: [FormsModule],
	exports: [InParagrafoComponent]
})
export class ComponentsModule {}
